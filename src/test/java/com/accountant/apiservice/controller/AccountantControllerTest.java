package com.accountant.apiservice.controller;

import com.accountant.apiservice.service.AccountantService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static com.accountant.apiservice.builder.ObjectTestBuilder.ACCOUNTANT_ID;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AccountantController.class)
public class AccountantControllerTest {

  @MockBean
  private AccountantService accountantService;

  @Autowired
  private MockMvc mvc;

  @Test
  public void deleteAccountantSuccess() throws Exception {
    mvc.perform(delete("/accountant/" + ACCOUNTANT_ID))
        .andExpect(status().isNoContent());

    verify(accountantService).delete(ACCOUNTANT_ID);
  }
}
