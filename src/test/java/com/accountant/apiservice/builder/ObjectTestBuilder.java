package com.accountant.apiservice.builder;

import com.accountant.apiservice.model.entities.Accountant;

public final class ObjectTestBuilder {

  private static final String ACCOUNTANT_NAME = "Accountant Name";
  private static final boolean ACCOUNTANT_ACTIVE = true;

  public static final long ACCOUNTANT_ID = 1L;

  private ObjectTestBuilder() {
    throw new UnsupportedOperationException();
  }

  public static Accountant buildAccountant() {
    Accountant accountant = new Accountant();
    accountant.setId(ACCOUNTANT_ID);
    accountant.setName(ACCOUNTANT_NAME);
    accountant.setActive(ACCOUNTANT_ACTIVE);
    return accountant;
  }


}
