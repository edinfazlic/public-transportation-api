package com.accountant.apiservice.service;

import com.accountant.apiservice.model.dto.AccountantDto;
import com.accountant.apiservice.model.entities.Accountant;
import com.accountant.apiservice.repository.AccountantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static com.accountant.apiservice.builder.ObjectTestBuilder.ACCOUNTANT_ID;
import static com.accountant.apiservice.builder.ObjectTestBuilder.buildAccountant;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

public class AccountantServiceTest {

  private AccountantRepository accountantRepository;
  private AccountantService accountantService;

  @BeforeEach
  public void before() {
    accountantRepository = Mockito.mock(AccountantRepository.class);
    accountantService = new AccountantService(accountantRepository);
  }

  @Test
  public void getSingleAccountantSuccess() {
    Accountant dbAccountant = buildAccountant();
    when(accountantRepository.findById(ACCOUNTANT_ID))
        .thenReturn(Optional.of(dbAccountant));

    AccountantDto accountant = accountantService.get(ACCOUNTANT_ID);

    assertNotNull(accountant);
    assertEquals(dbAccountant.getId(), accountant.getId());
    assertEquals(dbAccountant.getName(), accountant.getName());
    assertEquals(dbAccountant.isActive(), accountant.getEmployed());
  }

  @Test
  public void getSingleAccountantNotFound() {
    when(accountantRepository.findById(ACCOUNTANT_ID))
        .thenReturn(Optional.empty());

    assertThrows(RuntimeException.class, () -> accountantService.get(ACCOUNTANT_ID));
  }
}
